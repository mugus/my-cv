from django.conf.urls import url, include
from django.contrib import admin
import cvapp
urlpatterns = [
    url(r'',include('cvapp.urls')),
    url(r'^admin/', admin.site.urls)
]
