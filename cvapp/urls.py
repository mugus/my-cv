from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

app_name = 'cvapp'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^hire/$', views.hire, name='hire'),
    url(r'^about/$', views.about, name='about')
]
if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)